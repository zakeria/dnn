import torch
from torch import Tensor
from torch.nn import Linear, MSELoss, functional as F
import numpy as np

# The function to learn
def func(x):
    return x*x

def generate_data(batch_size):
    x = np.random.randint(-batch_size, batch_size, size=batch_size)
    y = func(x)

    inputs = np.column_stack([x, x**2])

    labels = y.reshape(batch_size, 1) 
    return inputs, labels

class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = Linear(2, 1)

        self.criterion = MSELoss()
        self.optimizer = torch.optim.Adam(self.parameters(), lr=0.01)

    def forward(self, x):
        x = self.fc1(x)
        return x

def train(model, epoch, batch_size):

    x, y = generate_data(batch_size)
    
    for e in range(epoch):
        res_y = model(Tensor(x))
        loss = model.criterion(res_y, Tensor(y))
        model.optimizer.zero_grad()
        loss.backward()
        model.optimizer.step()

        print("Loss, epoch: {},{}").format(e, loss / batch_size )


model = Net()

epoch = 1000
batch_size = 1000
train(model, epoch, batch_size)

pred = model(Tensor([5, 25]))
print(pred)
