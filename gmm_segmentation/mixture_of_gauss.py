import os 
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as spio
from scipy.stats import norm 
from scipy.stats import multivariate_normal
import time
import sys

def getGaussProb(x,mean,var):
    nDim = 3
    prob = np.exp(-0.5*((x-mean).T) @ np.linalg.inv(var) @ (x-mean))
    prob = prob/np.sqrt(np.linalg.det(var) * ((2*np.pi)**(nDim)))
    return prob

def getMixGaussLogLike(data, mixGaussEst): 
    data = np.atleast_2d(data)                                                                         

    # find total number of data items                                                                  
    nDims, nData = data.shape                                                                          
    
    # initialize log likelihoods                                                                       
    logLike = 0;                                                                                       
                                                                                                       
    # run through each data item                                                                       
    for cData in range(nData):                                                                         
        thisData = data[:, cData]                                                                      

        like = 0
        for k in range(mixGaussEst['k']):
            cov = mixGaussEst['cov'][:,:,k]
            mu = mixGaussEst['mean'][:,k]
            weight = mixGaussEst['weight'][k]            
            like = like + (weight * getGaussProb(thisData, mu, cov))
            
        # add to total log like
        logLike = logLike + np.log(like)                                                                      
                                                                                                       
    return  np.asscalar(logLike)   


def fitMixGauss(data, k):
    nDims, nData = data.shape

    postHidden = np.zeros(shape=(k, nData))

    mixGaussEst = dict()
    mixGaussEst['d'] = nDims
    mixGaussEst['k'] = k
    mixGaussEst['weight'] = (1 / k) * np.ones(shape=(k))
    mixGaussEst['mean'] = 2 * np.random.randn(nDims, k)
    mixGaussEst['cov'] = np.zeros(shape=(nDims, nDims, k))
    for cGauss in range(k):
        mixGaussEst['cov'][:, :, cGauss] = 2.5 + 1.5 * np.random.uniform() * np.eye(nDims)
        
    logLike = getMixGaussLogLike(data, mixGaussEst)
    print('Log Likelihood Iter 0 : {:4.3f}\n'.format(logLike))

    nIter = 25;

    logLikeVec = np.zeros(shape=(2 * nIter))
    boundVec = np.zeros(shape=(2 * nIter))

    fig, ax = plt.subplots(1, 1)

    for cIter in range(nIter):

        # ===================== =====================
        # E step
        # ===================== =====================

        for cData in range(nData):
            
            thisData = data[:, cData]
            for k in range(mixGaussEst['k']):
                cov = mixGaussEst['cov'][:,:,k]
                mu = mixGaussEst['mean'][:,k]
                weight = mixGaussEst['weight'][k] 

                # Calculate posterior        
                postHidden[k, cData] = weight * getGaussProb(thisData, mu, cov)
            
            # Normalise
            postHidden[:, cData] = postHidden[:, cData] / np.sum(postHidden[:, cData])
            

        # ===================== =====================
        # M Step
        # ===================== =====================
        # for each constituent Gaussian
        for cGauss in range(k):            
            # posterior probability associated with each Gaussian. Replace this:
            mixGaussEst['weight'][cGauss] = np.sum(postHidden[cGauss,:]) / np.sum(postHidden[:,:])

            mixGaussEst['mean'][:,cGauss] = data @ postHidden[cGauss, :] / np.sum(postHidden[cGauss,:]) 

            mu =  mixGaussEst['mean'][:,cGauss]
            newCov = np.zeros([3,3]) 
            for i in range(nData):
                thisData = data[:, i]
                xi = (thisData - mu).reshape(3,1) # force
                xi_t = (thisData - mu).reshape(1,3) # force (x_i-mu).T
                newCov = newCov + postHidden[cGauss, :][i] * (xi * xi_t)
            
            mixGaussEst['cov'][:, :, cGauss] = newCov / np.sum(postHidden[cGauss, :])
            
        time.sleep(0.7)
        fig.canvas.draw()

        logLike = getMixGaussLogLike(data, mixGaussEst)
        print('Log Likelihood After Iter {} : {:4.3f}\n'.format(cIter, logLike))

    return mixGaussEst

# Return a paired object
class PairObject(object):
  def __init__(self, obj1, obj2, obj3):
     self.obj1 = obj1
     self.obj2 = obj2
     self.obj3 = obj3
        
# Compute the ROC curve given our ground truth and posterior probabilities 
def computeRoc(pos, truth):
    # Create range of threshold values
    grid = np.arange(0.0, 1+ 0.025, 0.025)
    n = np.size(grid)
    
    TPs = np.zeros([n,1]) 
    FPs = np.zeros([n,1])
    
    T = rgb2gray(truth) 
    N  = (1 - T) 
    
    for idx, val in enumerate(grid):
        cond = np.where(pos > val, 1, 0)
        
        TP = T * cond
        FP = N * cond
        
        TPr = np.sum(TP) / np.sum(T)
        FPr = np.sum(FP) / np.sum(N)  
        
        TPs[idx] = TPr
        FPs[idx] = FPr

    return PairObject(TPs, FPs, grid)


def main():
    # Test image
    im = plt.imread('apples/Bbr98ad4z0A-ctgXo3gdwu8-scale.jpg').astype(np.float32) / 255

    appleClassifier = Model(paramsApple, paramsNon)
    posterior = classifyApple(im,appleClassifier)

    # Set up plots.
    f, (ax1, ax2, ax3) = plt.subplots(1, 3)
    #show the image

    ax1.imshow(im)
    ax1.set_title('Image')

    #show the ground truth mask
    gt = plt.imread('masks/Bbr98ad4z0A-ctgXo3gdwu8-scale.png').astype(np.float32)
    ax2.imshow(gt)
    ax2.set_title('Ground Truth')

    plt.show()

    # Posterior apple
    pos = res

    roc_res = computeRoc(pos,gt)
    TPs = roc_res.obj1
    FPs = roc_res.obj2
    grid = roc_res.obj3

    # Plot our results
    plt.plot(FPs,TPs, 'r')
    plt.plot(grid,grid, 'b')
    plt.title('ROC curve for apples')
    plt.ylabel('True positive rate')
    plt.xlabel('False positive rate')
    plt.show()
