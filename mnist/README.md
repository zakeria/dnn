# MNIST

MNIST handwritten digit recognition using PyTorch, with a classification accuracy of 99%.