import numpy as np
import torch
import torch.nn as nn

import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F

from torchvision import datasets, transforms

import torch.optim as optim

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 16, 3, 1)
        self.conv2 = nn.Conv2d(16, 32, 3, 1)

        self.dropout1 = nn.Dropout(0.1)
        self.dropout2 = nn.Dropout(0.5)

        self.fc1 = nn.Linear(4608, 64)
        self.fc2 = nn.Linear(64, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.dropout1(x)

        x = self.conv2(x)
        # print(x.size())
        x = F.relu(x)
        x = F.max_pool2d(x, 2, stride=2) 

        x = torch.flatten(x, start_dim=1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)

        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


def compute_data_std_mean(loader):
    data_mu = 0
    data_mu_sq = 0
    batches = 0
    for data, _ in loader:
        data_mu += torch.mean(data, dim=[0,2,3])
        data_mu_sq += torch.mean(data**2, dim=[0,2,3])

        batches+=1

    mu = data_mu / batches
    mu_sq = data_mu_sq / batches
    std = (mu_sq - mu**2)**0.5

    return mu, std

def train_net(model, device, loader, optimizer, epoch):
    for batch_idx, (data, label) in enumerate(loader):
        data, label = data.to(device), label.to(device)

        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, label)

        loss.backward()
        optimizer.step()

def predict(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, label in test_loader:
            data, label = data.to(device), label.to(device)
            output = model(data)

            pred = output.argmax(dim=1, keepdim=True)  
            correct += pred.eq(label.view_as(pred)).sum().item()

    percent_correct = 100. * correct / len(test_loader.dataset) 
    return percent_correct


def mnist_example():
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
        ])

    mnist_data = datasets.MNIST('dataset', train=True, download=True, transform=transform)

    mnist_data_test = datasets.MNIST('dataset', train=False, download=True, transform=transform)

    train_loader = torch.utils.data.DataLoader(mnist_data, batch_size=128, shuffle=True)
    test_loader = torch.utils.data.DataLoader(mnist_data_test, batch_size=1000, shuffle=True)

    # mu, std = compute_data_std_mean(train_loader)

    device = torch.device("cuda")
    model = Net().to(device)

    epoch = 15 
    learn_rate = 1.
    optimizer = optim.Adadelta(model.parameters(), lr=learn_rate)

    for epoch in range(epoch):
        train_net(model, device, train_loader, optimizer, epoch)
        accuracy = predict(model, device, test_loader)
        print('Epoch: {}, accuracy: {}'.format(epoch, accuracy))

mnist_example()
